namespace :phones do
  desc "populate db with phones"
  task :base => :environment do
    json = File.read('lib/phones.json')
    phones = JSON.parse(json)

    Phone.delete_all
    phones.each do |phone|
      Phone.create(name: phone['id'], description: "#{Faker::Lorem.sentence}:::#{Faker::Lorem.sentence}")
    end
  end


end