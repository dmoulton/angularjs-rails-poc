class PhonesController < ApplicationController
  respond_to :json, :html

  def index
    @phone = Phone.new
    @phones = Phone.all
  end

  def create
    params[:phone][:description] = "#{params[:phone][:f1]}:::#{params[:phone][:f2]}"
    phone = Phone.create(params[:phone])
    respond_with phone
  end

  def update
    phone = Phone.find(params[:id])
    params[:phone][:description] = "#{params[:phone][:f1]}:::#{params[:phone][:f2]}"
    phone.update_attributes(params[:phone])
    respond_with phone
  end

  def destroy
    phone = Phone.find(params[:id])
    phone.destroy
    respond_with phone
  end
end
