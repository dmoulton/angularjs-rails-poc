app = angular.module('Phone', ['ngResource'])

@PhonesCtrl = ($scope, $resource) ->
  Phone = $resource("/phones/:id", {id: "@id"}, {update: {method: "PUT"}})
  $scope.phones = Phone.query()
  $scope.orderProp = 'name';

  $scope.addPhone = ->
    $scope.phones.push($scope.newPhone)
    Phone.save($scope.newPhone)
    $scope.newPhone = {}
    $('#newModal').modal('toggle')

  $scope.editThisPhone = (phone) ->
    $scope.editPhone = phone
    $scope.orig_phone = angular.copy(phone)
    $('#editModal').modal('toggle')

  $scope.updatePhone = ->
    Phone.update($scope.editPhone)
    $scope.phone = $scope.editPhone
    $scope.editPhone = {}
    $('#editModal').modal('toggle')

  $scope.showThisPhone = (phone) ->
    $scope.showPhone = phone
    $('#showModal').modal('toggle')

  $scope.resetThisPhone = (phone) ->
    $scope.phones.pop(phone)
    $scope.phones.push($scope.orig_phone)

  $scope.deleteThisPhone = (phone) ->
    Phone.remove(id: phone.id)
    $scope.phones.pop(phone)