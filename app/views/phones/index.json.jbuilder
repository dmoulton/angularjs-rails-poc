json.array!(@phones) do |phone|
    json.id phone.id
    json.name phone.name
    json.f1 phone.f1
    json.f2 phone.f2
end