class Phone < ActiveRecord::Base
  attr_accessible :description, :name, :f1, :f2
  attr_writer :f1, :f2

  def f1
    self.description.split(":::")[0]  rescue nil
  end

  def f2
    self.description.split(":::")[1] rescue nil
  end
end
